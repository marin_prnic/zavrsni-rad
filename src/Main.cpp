#include <Windows.h>
#include <iostream>
#include <GL\glew.h>
#include <GL\glut.h>
#include "Game.h"

using namespace std;

Game game;

void init()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_SMOOTH);
}

void BitmapText(char* str, int wcx, int wcy)
{
	glRasterPos2i(wcx, wcy);
	for (int i = 0; str[i] != '\0'; i++) {
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, str[i]);
	}
}

void timer(int id)
{
	if (game.killed) {

		game.paused = true;
		game.timer = 1000;
		game.clearMainGrid();
		game.clearNextPieceGrid();

	}
	else if (!game.paused) {	
		game.update();
		switch (game.level) {
		case 1:
			//mciSendString(L"stop  lvl5", NULL, 0, NULL);
			//mciSendString(L"seek lvl5 to start", NULL, 0, NULL);
			mciSendString(L"play lvl1 repeat", NULL, 0, NULL);
			game.timer = 1000;
			break;
		case 2:
			game.timer = 900;
			break;
		case 3:
			game.timer = 800;
			mciSendString(L"stop  lvl1", NULL, 0, NULL);
			mciSendString(L"seek lvl1 to start", NULL, 0, NULL);
			mciSendString(L"play lvl2 repeat", NULL, 0, NULL);
			break;
		case 4:
			game.timer = 700;
			break;
		case 5:
			game.timer = 600;
			mciSendString(L"stop  lvl2", NULL, 0, NULL);
			mciSendString(L"seek lvl2 to start", NULL, 0, NULL);
			mciSendString(L"play lvl3 repeat", NULL, 0, NULL);
			break;
		case 6:
			game.timer = 500;
			break;
		case 7:
			game.timer = 400;
			mciSendString(L"stop  lvl3", NULL, 0, NULL);
			mciSendString(L"seek lvl3 to start", NULL, 0, NULL);
			mciSendString(L"play lvl4 repeat", NULL, 0, NULL);
			break;
		case 8:
			game.timer = 300;
			break;
		case 9:
			mciSendString(L"stop  lvl4", NULL, 0, NULL);
			mciSendString(L"seek lvl4 to start", NULL, 0, NULL);
			mciSendString(L"play lvl5 repeat", NULL, 0, NULL);
			game.timer = 200;
			break;
		case 10:
			game.timer = 100;
			break;

		}
		glutTimerFunc(game.timer, timer, 0);
	}
	glutPostRedisplay();
}


void keyboard(unsigned char key, int x, int y)
{
	if (game.paused && game.killed) {
		if (key == 13) { // 13 == ENTER
			game.killed = false;
			game.restart();
			if (game.level < 3)
				mciSendString(L"play lvl1 repeat", NULL, 0, NULL);
			else if (game.level < 5)
				mciSendString(L"play lvl2 repeat", NULL, 0, NULL);
			else if (game.level < 7)
				mciSendString(L"play lvl3 repeat", NULL, 0, NULL);
			else if (game.level < 9)
				mciSendString(L"play lvl4 repeat", NULL, 0, NULL);
			else if (game.level > 9)
				mciSendString(L"play lvl5 repeat", NULL, 0, NULL);

			glutTimerFunc(game.timer, timer, 0);
		}
	}
	else {
		if (key == 27) { // 27 == ESCAPE
			if (game.level < 3)
				mciSendString(L"play lvl1 repeat", NULL, 0, NULL);
			else if (game.level < 5)
				mciSendString(L"play lvl2 repeat", NULL, 0, NULL);
			else if (game.level < 7)
				mciSendString(L"play lvl3 repeat", NULL, 0, NULL);
			else if (game.level < 9)
				mciSendString(L"play lvl4 repeat", NULL, 0, NULL);
			else if (game.level > 9)
				mciSendString(L"play lvl5 repeat", NULL, 0, NULL);
			game.paused = !game.paused;
			glutTimerFunc(game.timer, timer, 0);
		}
		if (key == 'r') {
			mciSendString(L"stop  lvl1", NULL, 0, NULL);
			mciSendString(L"stop  lvl2", NULL, 0, NULL);
			mciSendString(L"stop  lvl3", NULL, 0, NULL);
			mciSendString(L"stop  lvl4", NULL, 0, NULL);
			mciSendString(L"stop  lvl5", NULL, 0, NULL);

			mciSendString(L"seek lvl1 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl2 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl3 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl4 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl5 to start", NULL, 0, NULL);
			game.restart();
		}
	}

	glutPostRedisplay();
}


void special(int key, int x, int y)
{
	if (!game.paused && !game.killed) {
		if (key == GLUT_KEY_LEFT) {
			game.move(-1);
		}
		else if (key == GLUT_KEY_RIGHT) {
			game.move(1);
		}
		else if (key == GLUT_KEY_UP) {
			game.rotateShape(1);
		}
		else if (key == GLUT_KEY_DOWN) {
			game.update();
		}
	}
	glutPostRedisplay();
}


void display(void)
{

	GLfloat light_Intensity[] = { 1,1,1,20 };
	GLfloat light_Position[] = { 0,0,0,100 };

	glLightfv(GL_LIGHT0, GL_POSITION, light_Position);
	glLightfv(GL_LIGHT0, GL_AMBIENT_AND_DIFFUSE, light_Intensity);
	const int N = 100;
	char msg[N + 1];

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glViewport(margins, margins, VPWIDTH, VPHEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (!game.paused) {	

		gluPerspective(90, 430 / 330, 1, 41);
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glOrtho(0, COLS, ROWS, 0, 1, BLOCKSIZE+1);
		
		
		for (int i = 0; i < COLS; i++) {
			for (int j = 0; j < ROWS; j++) {
				glPushMatrix();
				glColor4f(1.0, 1.0, 1.0, 0.5);
				glTranslatef(.5 + i, .5 + j, 1);
				glutWireCube(1);
				glPopMatrix();
			}
		}
		glutPostRedisplay();
		
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLS; c++) {
				Square& square = game.mainGrid[r][c];
				if (square.isFilled) {

					glPushMatrix();
					glColor3f(square.red, square.green, square.blue);
					glTranslatef(c +.5, r+.5, 1.0f);
					glutSolidCube(.8);
					glPopMatrix();
				}
			}
		}
		glutPostRedisplay();
	}
	else {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glOrtho(0, VPWIDTH, 0, VPHEIGHT,0,BLOCKSIZE+1);

		if (game.paused && !game.killed) {		
			glColor3f(1, 1, 1);
			sprintf_s(msg, N, "GAME PAUSED");
			BitmapText(msg, 140, VPHEIGHT / 2);
			mciSendString(L"pause lvl1", NULL, 0, NULL);
			mciSendString(L"pause lvl2", NULL, 0, NULL);
			mciSendString(L"pause lvl3", NULL, 0, NULL);
			mciSendString(L"pause lvl4", NULL, 0, NULL);
			mciSendString(L"pause lvl5", NULL, 0, NULL);

		}
		if (game.paused && game.killed) {		
			glColor3f(1, 1, 1);
			sprintf_s(msg, N, "GAME OVER");
			BitmapText(msg, 155, VPHEIGHT / 2 + 50);
			sprintf_s(msg, N, "YOUR SCORE: %d", game.score);
			BitmapText(msg, 140, VPHEIGHT / 2);
			sprintf_s(msg, N, "Press [ENTER] to restart ...");
			BitmapText(msg, 75, VPHEIGHT / 2 - 100);
			mciSendString(L"stop  lvl1", NULL, 0, NULL);
			mciSendString(L"stop  lvl2", NULL, 0, NULL);
			mciSendString(L"stop  lvl3", NULL, 0, NULL);
			mciSendString(L"stop  lvl4", NULL, 0, NULL);
			mciSendString(L"stop  lvl5", NULL, 0, NULL);

			mciSendString(L"seek lvl1 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl2 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl3 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl4 to start", NULL, 0, NULL);
			mciSendString(L"seek lvl5 to start", NULL, 0, NULL);
		}
	}


	glViewport(VPWIDTH, 0, VPWIDTH, VPHEIGHT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glOrtho(0, VPWIDTH, 0, VPHEIGHT,0,BLOCKSIZE+1);;

	glColor3f(1, 1, 1);

	sprintf_s(msg, N, "Level");
	BitmapText(msg, 50, 320);

	sprintf_s(msg, N, "%d", game.level);
	BitmapText(msg, 100, 295);

	sprintf_s(msg, N, "Score");
	BitmapText(msg, 50, 245);

	sprintf_s(msg, N, "%d", game.score);
	BitmapText(msg, 100, 220);

	sprintf_s(msg, N, "Lines Cleared");
	BitmapText(msg, 50, 170);

	sprintf_s(msg, N, "%d", game.linesCleared);
	BitmapText(msg, 100, 145);

	sprintf_s(msg, N, "Shapes Encountered");
	BitmapText(msg, 50, 95);

	sprintf_s(msg, N, "%d", game.shapesCount);
	BitmapText(msg, 100, 70);

	sprintf_s(msg, N, "Next Shape:");
	BitmapText(msg, 50, VPHEIGHT - 30);

	glutPostRedisplay();

	
	glViewport(VPWIDTH + 40, -20, VPWIDTH, VPHEIGHT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glOrtho(0, COLS, ROWS, 0,1,BLOCKSIZE+1);

	for (int r = 1; r < 5; r++) {
		for (int c = 0; c < 2; c++) {
			Square& square = game.nextPieceGrid[r][c];
			if (square.isFilled) {
				glColor3f(square.red, square.green, square.blue);
				glRectd(c + .1, r + .1, c + .9, r + .9);
			}
		}
	}
	glutPostRedisplay();
	
	
	glutSwapBuffers();
}

void main(int argc, char *argv[])
{
	mciSendString(L"open \"lvl1.wav\" type mpegvideo alias lvl1", NULL, 0, NULL);
	mciSendString(L"open \"lvl2.wav\" type mpegvideo alias lvl2", NULL, 0, NULL);
	mciSendString(L"open \"lvl3.wav\" type mpegvideo alias lvl3", NULL, 0, NULL);
	mciSendString(L"open \"lvl4.wav\" type mpegvideo alias lvl4", NULL, 0, NULL);
	mciSendString(L"open \"lvl5.wav\" type mpegvideo alias lvl5", NULL, 0, NULL);
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

	glutInitWindowPosition(100, 100);
	glutInitWindowSize(VPWIDTH + VPWIDTH/2 + 2 * margins, VPHEIGHT + 2 * margins);

	glutCreateWindow("Tetris");
	init();

	glutDisplayFunc(display);
	glutSpecialFunc(special);
	glutKeyboardFunc(keyboard);
	glutTimerFunc(game.timer, timer, 0);

	mciSendString(L"play lvl1 repeat", NULL, 0, NULL);
	glutMainLoop();
}
