#include <iostream>
#include <random>
#include <Windows.h>
#include <GL\glew.h>
#include <GL\freeglut.h>
#include "Game.h"

using namespace std;

int random()
{
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> distr(0, 6);
	return distr(gen);
}


void Game::update() {
	if (moveCollision(0)) {
		if (activePiece.y < 3) {			
			killed = true;
			mciSendString(L"seek game_over to start", NULL, 0, NULL);
			mciSendString(L"play game_over", NULL, 0, NULL);
		}
		else {								
			updateActiveAfterCollision();	
			checkLine();					
			if (deleteLines)			
				clearLine();				
			genNextPiece();					

			clearNextPieceGrid();
			updateNextPieceGrid();

			updateActivePiece();			
		}
	}
	else {
		fixActivePiece();
		activePiece.y++;
		updateActivePiece();
	}
}


void Game::restart()
{
	clearMainGrid();		
	clearNextPieceGrid();	
	score = 0;				
	level = 1;				
	linesCleared = 0;		
	shapesCount = 1;		
	killed = false;
	paused = false;
	deleteLines = false;
	timer = 1000;

	activePiece = Piece(random());
	activePiece.x = COLS/2;
	activePiece.y = 0;
	updateActivePiece();


	nextPiece = Piece(random());
	nextPiece.x = COLS/2;
	nextPiece.y = 0;
	updateNextPieceGrid();

}


void Game::fixActivePiece() {
	const int* trans = activePiece.rotations();
	for(int i = 0; i < 8; i += 2){
		Square &square = mainGrid[activePiece.y + trans[i + 1]][activePiece.x + trans[i]];
		square.isFilled = false;
		square.isActive = false;
	}
}

void Game::genNextPiece() {
	activePiece = nextPiece;
	nextPiece = Piece(random());
	nextPiece.x = COLS/2;
	nextPiece.y = 0;
	shapesCount++;
}


void Game::move(int dir)
{
	if(moveCollision(dir))
		return;				
	fixActivePiece();
	activePiece.x += dir;
	updateActivePiece();
}


void Game::clearMainGrid()
{
	for (int r=0; r<ROWS; r++) {
		for (int c=0; c<COLS; c++) {
			mainGrid[r][c].isFilled = false;
			mainGrid[r][c].isActive = false;
		}
	}
}

void Game::clearNextPieceGrid()
{
	for (int r = 0; r < 5; r++) {	
		for (int c = 0; c < 5; c++) {
			nextPieceGrid[r][c].isFilled = false;
			nextPieceGrid[r][c].isActive = false;
		}
	}
}

void Game::updateActivePiece() {
	const int* trans = activePiece.rotations();
	for(int i = 0; i < 8; i += 2){
		Square &square = mainGrid[activePiece.y + trans[i + 1]][activePiece.x + trans[i]];
		square.isFilled = true;
		square.isActive = true;
		square.red = activePiece.redVal;
		square.green = activePiece.blueVal;
		square.blue = activePiece.greenVal;
	}	
}

void Game::updateNextPieceGrid() {
	const int* transNext = nextPiece.rotations();
	for (int i = 0; i < 8; i += 2) {
		Square& squareNext = nextPieceGrid[nextPiece.y + transNext[i + 1]][nextPiece.x + transNext[i]];
		squareNext.isFilled = true;
		squareNext.isActive = true;
		squareNext.red = nextPiece.redVal;
		squareNext.green = nextPiece.blueVal;
		squareNext.blue = nextPiece.greenVal;
	}
}

Game::Game()
{
	mciSendString(L"open \"new_level.wav\" type mpegvideo alias new_level", NULL, 0, NULL);
	mciSendString(L"open \"line_clear.wav\" type mpegvideo alias line_clear", NULL, 0, NULL);
	mciSendString(L"open \"tetris_tup.wav\" type mpegvideo alias tup", NULL, 0, NULL);
	mciSendString(L"open \"Game_over.wav\" type mpegvideo alias game_over", NULL, 0, NULL);
	restart();
}

void Game::rotateShape(int dir) {
	activePieceCopy = Piece(random());
	activePieceCopy.x = activePiece.x;
	activePieceCopy.y = activePiece.y;
	activePieceCopy.rotation = activePiece.rotation;
	activePieceCopy.type = activePiece.type;
	activePieceCopy.rotatePiece(dir);

	if(canRotate(activePieceCopy)) {
		fixActivePiece();
		activePiece.rotatePiece(dir);
		updateActivePiece();
	}
}


bool Game::canRotate(Piece activeP) {
	if(rotationCollision()) {
		return false;
	}
	else
	{
		mciSendString(L"seek tup to start", NULL, 0, NULL);
		mciSendString(L"play tup", NULL, 0, NULL);
		return true;
	}
}


bool Game::rotationCollision() {
	int x, y;
	const int* trans = activePieceCopy.rotations();
	for (int i = 0; i < 8; i += 2) {
		x = activePieceCopy.x + trans[i];
		y = activePieceCopy.y + trans[i + 1];

		if (x >= COLS || y >= ROWS || x < 0 || (mainGrid[y][x].isFilled && !mainGrid[y][x].isActive))
			return true;
	}
	return false;
}


bool Game::moveCollision(int dir) {
	int x, y;
	const int* trans = activePiece.rotations();
	for (int i = 0; i < 8; i += 2) {
		x = activePiece.x + trans[i];
		y = activePiece.y + trans[i + 1];
		if (dir == 0)
			y += 1;
		else
			x += dir;
		if (x >= COLS || y >= ROWS || x < 0 || (mainGrid[y][x].isFilled && !mainGrid[y][x].isActive))
			return true;
	}
	return false;
}


void Game::updateActiveAfterCollision() {
	const int* trans = activePiece.rotations();
	for(int i = 0; i < 8; i += 2){
		Square &square = mainGrid[activePiece.y + trans[i + 1]][activePiece.x + trans[i]];
		square.isActive = false;
	}
}


void Game::checkLine() {
	int curr_level = level;
	int fullRows = 0;
	for (int r=0; r<ROWS; r++) {
		bool fullRow = false;
			for (int c=0; c<COLS; c++) {
				Square &square = mainGrid[r][c];
				if (square.isFilled){
					fullRow = true;
				}
				else {
					fullRow = false;
					break;
				}
			}
		if(fullRow) {
			for ( int c =0; c < COLS; c++){
				mainGrid[r][c].toBeDeleted = true;
			}
			deleteLines = true;
			linesCleared++;
			fullRows++;
		}
		else
		{
			mciSendString(L"seek tup to start", NULL, 0, NULL);
			mciSendString(L"play tup", NULL, 0, NULL);
		}
	}
	switch (fullRows)
	{
		case 1:
			score += 100;
			break;
		case 2:
			score += 200;
			break;
		case 3:
			score += 400;
			break;
		case 4:
			score += 500;
			break;
	}

	if (score >= curr_level * 1000 && score <10000)
	{
		mciSendString(L"seek new_level to start", NULL, 0, NULL);
		mciSendString(L"play new_level", NULL, 0, NULL);
		level++;
	}
}


void Game::clearLine() {
	mciSendString(L"seek line_clear to start", NULL, 0, NULL);
	mciSendString(L"play line_clear", NULL, 0, NULL);
	for (int r = ROWS-1; r > 0; r--){ 
		if (mainGrid[r][0].toBeDeleted){
			for (int r2 = r; r2>0; r2--){ 
				for (int c = 0; c < COLS; c++){
					mainGrid[r2][c].isFilled=mainGrid[r2-1][c].isFilled;
					mainGrid[r2][c].isActive=mainGrid[r2-1][c].isActive;
					mainGrid[r2][c].toBeDeleted=mainGrid[r2-1][c].toBeDeleted;
					mainGrid[r2][c].red=mainGrid[r2-1][c].red;
					mainGrid[r2][c].green=mainGrid[r2-1][c].green;
					mainGrid[r2][c].blue=mainGrid[r2-1][c].blue;
				}
			}
			r++;
		}
	}
	deleteLines = false;

}
